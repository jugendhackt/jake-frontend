import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:jake/drawer.dart';

class InfoPage extends StatelessWidget {
  InfoPage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      drawer: JakeDrawer(),
      appBar: AppBar(title: Text("Information")),
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Text(
                    "Groups",
                    style: Theme.of(context).textTheme.title,
                  ),
                  Text(
                      "Groups are a useful feature in this smart calendar. You can easily find a date where everybody can come and add this date to everybody's calendar!"),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Text(
                    "Places",
                    style: Theme.of(context).textTheme.title,
                  ),
                  Text(
                      "Places helps you to find a group and has the security that the room is free. You can create a place, can add member which can have different permission and see in the plan, if the room is free!"),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Text("App", style: Theme.of(context).textTheme.title),
                  Text(
                    "GitLab: https://gitlab.com/vangora/jake \r\n"
                    "Version: Alpha 0.0.1 \r\n"
                    "Contributors: CodeDoctorDE \r\n"
                    "Current platform: unknown",
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
