import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:jake/drawer.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class GroupsPage extends StatefulWidget {
  GroupsPage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  GroupsPageState createState() => GroupsPageState();
}

class GroupsPageState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      drawer: JakeDrawer(),
      appBar: AppBar(title: Text("Groups")),
      body: Center(
        child: Column(
          children: <Widget>[],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: "Create group",
        child: Icon(MdiIcons.plus),
        onPressed: () {},
      ),
    );
  }
}
