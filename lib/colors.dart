import "package:flutter/material.dart";

const PRIMARY = Color.fromRGBO(175, 20, 75, 1);
const PRIMARY_TEXT = Color.fromRGBO(200, 200, 200, 1);
const SECONDARY = Color.fromRGBO(150, 150, 150, 1);
