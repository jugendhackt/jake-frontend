import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jake/info.dart';
import 'package:jake/main.dart';
import 'package:jake/places/main.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class JakeDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            height: 250,
            child: (
                DrawerHeader(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SvgPicture.asset(
                          "assets/icon.svg",
                          width: 100,
                          height: 100
                      ),
                      Text(
                          'Jake',
                          style: Theme
                              .of(context)
                              .textTheme
                              .headline
                      ),
                      Text(
                          'proficodergames@gmail.com',
                          style: Theme
                              .of(context)
                              .textTheme
                              .subhead
                      ),
                      Text(
                          'Alpha 0.0.1',
                          style: Theme
                              .of(context)
                              .textTheme
                              .subtitle
                      ),
                    ],
                  ),
                )),
          ),
          ListTile(
            title: Text('Home'),
            leading: Icon(MdiIcons.home),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomePage()),
              );
            },
          ),
          ListTile(
            title: Text('Groups'),
            leading: Icon(MdiIcons.accountGroup),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomePage()),
              );
            },
          ),
          ListTile(
            title: Text('Places'),
            leading: Icon(MdiIcons.grid),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PlacesPage()),
              );
            },
          ),
          Divider(),
          ListTile(
            title: Text('Servers'),
            leading: Icon(MdiIcons.server),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => InfoPage()),
              );
            },
          ),
          ListTile(
            title: Text('Info'),
            leading: Icon(MdiIcons.informationOutline),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => InfoPage()),
              );
            },
          ),
          ListTile(
            title: Text('Settings'),
            leading: Icon(MdiIcons.settingsOutline),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => InfoPage()),
              );
            },
          ),
        ],
      ),
    );
  }
}
